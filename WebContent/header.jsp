<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div>
	<a href="${request.getContextPath()}/listProduct">Products</a>
	<c:choose>
		<c:when test="${request.getSession().getAttribute('username') != null}">
			<a href="${request.getContextPath()}/addProduct">Add product</a>
			<a href="${request.getContextPath()}/logout">Logout</a>
		</c:when>
		<c:otherwise>
			<a href="${request.getContextPath()}/login">Login</a>
		</c:otherwise>
	</c:choose>
</div>