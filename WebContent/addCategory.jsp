<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add category</title>
</head>
<body>
	<h1>New category</h1>
	<form action="addCategory" method="post">
		<input type="text" name="name" placeholder="Name"/>
		<input type="submit" value="Add category"/>
	</form>
	<c:if test="${not empty lastAdded}">
		<div>
			<strong>Last added category:</strong>&nbsp;${lastAdded}
		</div>
	</c:if>
</body>
</html>