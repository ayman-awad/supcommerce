<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add product</title>
</head>
<body>
	<%@include file="../header.jsp" %>
	<h1>Add product</h1>
	<form action="addProduct" method="post">
		<div>
			<input type="text" name="name" placeholder="Name"/>
		</div>
		<div>
			<textarea rows="5" name="content" placeholder="Content"></textarea>
		</div>
		<div>
			<input type="number" name="price" placeholder="Price"/>
		</div>
		<div>
			<label for="category_id">Category</label>
			<select name="category_id">
				<c:forEach items="${categories}" var="category">
					<option value="${category.id}">${category.name}</option>
				</c:forEach>
			</select>
		</div>
		<div>
			<button type="submit">Add</button>
		</div>
	</form>
	<%@include file="../footer.jsp" %>
</body>
</html>