<%@page import="com.supinfo.sun.supcommerce.bo.SupProduct"%>
<%@page import="com.supinfo.sun.supcommerce.doa.SupProductDao"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>
	<%@include file="header.jsp" %>
	<h1>Products</h1>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Name</td>
				<td>Content</td>
				<td>Price</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${products}" var="product">
				<tr>
					<td><c:out value="${product.getId()}"/></td>
					<td><c:out value="${product.getName()}"/></td>
					<td><c:out value="${product.getContent()}"/></td>
					<td><c:out value="${product.getPrice()}"/></td>
					<td>
					<form action="${request.getContextPath()}/removeProduct" method="post">
						<input type="hidden" name="id" value="${product.getId() }"/>
						<input type="submit" value="Remove">
					</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<%@include file="footer.jsp" %>
</body>
</html>