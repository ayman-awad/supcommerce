<%@page import="com.supinfo.sun.supcommerce.exception.UnknownProductException"%>
<%@page import="com.supinfo.sun.supcommerce.bo.SupProduct"%>
<%@page import="com.supinfo.sun.supcommerce.doa.SupProductDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
	<form action="login" method="post">
		<input type="text" name="username" placeholder="Username"/>
		<button type="submit">Login</button>
	</form>
</body>
</html>