<%@page import="com.supinfo.sun.supcommerce.exception.UnknownProductException"%>
<%@page import="com.supinfo.sun.supcommerce.bo.SupProduct"%>
<%@page import="com.supinfo.sun.supcommerce.doa.SupProductDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product</title>
</head>
<body>
	<%@include file="header.jsp" %>
	
	<h1>Product details</h1>
	
	<c:if test="${not empty product}">
		<div><strong>Id:</strong>${product.getId()}</div>
		<div><strong>Name:</strong>${product.getName()}</div>
		<div><strong>Content:</strong>${product.getContent()}</div>
		<div><strong>Price:</strong>${product.getPrice()}&euro;</div>
		<div><strong>Category:</strong>${product.category.name}</div>
	</c:if>
	
	<c:if test="${not empty error}">
		${error}
	</c:if>
	
	<%@include file="footer.jsp" %>
</body>
</html>