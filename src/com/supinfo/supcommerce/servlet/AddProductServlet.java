package com.supinfo.supcommerce.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.dao.DaoFactory;
import com.supinfo.supcommerce.entity.Category;
import com.supinfo.supcommerce.entity.SupProduct;

/**
 * Servlet implementation class ShowProductServlet
 */
@WebServlet(name="AddProductServlet", urlPatterns={"/addProduct"})
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	EntityManagerFactory emf;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Category> categories = DaoFactory.getCategoryDao().getAllCategories();
		request.setAttribute("categories", categories);
		getServletContext().getRequestDispatcher("/auth/addProduct.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String name = request.getParameter("name");
		String content = request.getParameter("content");
		String price = request.getParameter("price");
		String categoryId = request.getParameter("category_id");
		
		if(!name.isEmpty() && !content.isEmpty() && !price.isEmpty()) {
			
			try {
				float priceFloat = Float.parseFloat(price);
				
				SupProduct product = new SupProduct();
		    	product.setName(name);
		    	product.setContent(content);
		    	product.setPrice(priceFloat);
		    	
		    	if(!categoryId.isEmpty()) {
		    		try {
		    			long categoryIdLong = Long.parseLong(categoryId);
		    			Category category = DaoFactory.getCategoryDao().findCategoryById(categoryIdLong);
		    			if(category != null) {
		    				product.setCategory(category);
		    			}
		    		}
		    		catch (NumberFormatException e) {
						// TODO: handle exception
					}
		    	}
		    	
				DaoFactory.getProductDao().addProduct(product);
		    	
		    	((HttpServletResponse) response).sendRedirect(((HttpServletRequest)request).getContextPath() + "/showProduct?id=" + product.getId());
			}
			catch (NumberFormatException e) {
				// TODO display error message to user
				doGet(request, response);
			}
		}
		else {
			doGet(request, response);
		}
	}

}
