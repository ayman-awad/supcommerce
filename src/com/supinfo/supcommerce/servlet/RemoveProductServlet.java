package com.supinfo.supcommerce.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.dao.DaoFactory;

/**
 * Servlet implementation class RemoveProductServlet
 */
@WebServlet(name="RemoveProductServlet", urlPatterns={"/removeProduct"})
public class RemoveProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		if(!id.isEmpty()) {
			try {
				long idLong = Long.parseLong(id);
		    	DaoFactory.getProductDao().removeProduct(idLong);
			}
			catch (NumberFormatException e) {
				// TODO display error message to user
			}
		}
		((HttpServletResponse) response).sendRedirect(((HttpServletRequest)request).getContextPath() + "/listProduct");
	}

}
