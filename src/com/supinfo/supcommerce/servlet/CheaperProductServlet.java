package com.supinfo.supcommerce.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.entity.SupProduct;

/**
 * Servlet implementation class CheaperProductServlet
 */
@WebServlet(name = "CheaperProductServlet", urlPatterns={"/listCheaper", "/cheaper"})
public class CheaperProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	EntityManagerFactory emf;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheaperProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void init() throws ServletException {
		this.emf = Persistence.createEntityManagerFactory("sc_pu");
		super.init();
	}
	
	@Override
	public void destroy() {
		emf.close();
		super.destroy();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO a method to do this in ProductDao
		EntityManager em = emf.createEntityManager();
		List<SupProduct> products = em.createQuery("SELECT p FROM SupProduct AS p WHERE p.price < 100").getResultList();
		em.close();
		request.setAttribute("products", products);
		getServletContext().getRequestDispatcher("/listProduct.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
