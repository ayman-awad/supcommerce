package com.supinfo.supcommerce.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.dao.DaoFactory;
import com.supinfo.supcommerce.entity.SupProduct;

/**
 * Servlet implementation class ShowProductServlet
 */
@WebServlet(name="ShowProductServlet", urlPatterns={"/showProduct"})
public class ShowProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String stringId = request.getParameter("id");
		if(stringId != null && !stringId.isEmpty()) {
			try {
				long id = Long.parseLong(stringId);
				SupProduct product = DaoFactory.getProductDao().findProductById(id);
				request.setAttribute("product", product);
			}
			catch(NumberFormatException e) {
				request.setAttribute("error", "Missing or invalid id.");
			}
		}
		
		getServletContext().getRequestDispatcher("/showProduct.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
