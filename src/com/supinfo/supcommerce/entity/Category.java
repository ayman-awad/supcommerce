package com.supinfo.supcommerce.entity;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity
@Table(name="categories")
@XmlRootElement(name = "category")
public class Category implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy="category", fetch=FetchType.EAGER)
	private List<SupProduct> products;

	public Category() {
		super();
	}
	
	@XmlAttribute
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElementWrapper(name="products")
	@XmlElement(name="product")
	public List<SupProduct> getProducts() {
		return products;
	}

	public void setProducts(List<SupProduct> products) {
		this.products = products;
	}
}
