package com.supinfo.supcommerce.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.supinfo.supcommerce.dao.ProductDao;
import com.supinfo.supcommerce.entity.SupProduct;

public class JpaProductDao implements ProductDao {
	
	EntityManagerFactory emf;
	
	public JpaProductDao(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public List<SupProduct> getAllProducts() {
		EntityManager em = emf.createEntityManager();
		List<SupProduct> products = em.createQuery("SELECT p FROM SupProduct AS p").getResultList();
		em.close();
		return products;
	}

	@Override
	public SupProduct addProduct(SupProduct product) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		try {
			transaction.begin();
			em.persist(product);
			em.flush();
			transaction.commit();
		}
		finally {
			if(transaction.isActive()) {
				transaction.rollback();
			}
			em.close();
		}
		return product;
	}

	@Override
	public void removeProduct(Long id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
    	em.createQuery("DELETE FROM SupProduct p WHERE p.id = :id").setParameter("id", id).executeUpdate();
    	em.getTransaction().commit();
    	em.close();
	}
	
	@Override
	public void removeProduct(SupProduct product) {
		removeProduct(product.getId());
	}

	@Override
	public SupProduct findProductById(Long id) {
		EntityManager em = emf.createEntityManager();
		SupProduct product = (SupProduct) em.createQuery("SELECT p FROM SupProduct AS p WHERE p.id = :id").setParameter("id", id).getSingleResult();
		em.close();
		return product;
	}

}
