package com.supinfo.supcommerce.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.supinfo.supcommerce.dao.CategoryDao;
import com.supinfo.supcommerce.entity.Category;

public class JpaCategoryDao implements CategoryDao {

	private EntityManagerFactory emf;
	
	public JpaCategoryDao(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public List<Category> getAllCategories() {
		EntityManager em = emf.createEntityManager();
		List<Category> categories = em.createQuery("SELECT c FROM Category AS c").getResultList();
		em.close();
		return categories;
	}

	@Override
	public Category addCategory(Category category) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		try {
			transaction.begin();
			em.persist(category);
			em.flush();
			transaction.commit();
		}
		finally {
			if(transaction.isActive()) {
				transaction.rollback();
			}
			em.close();
		}
		return category;
	}

	@Override
	public void removeCategory(Long id) {
		EntityManager em = emf.createEntityManager();
    	em.createQuery("DELETE FROM Category AS c WHERE c.id = :id").setParameter("id", id).executeUpdate();
    	em.close();
	}

	@Override
	public void removeCategory(Category category) {
		removeCategory(category.getId());
	}

	@Override
	public Category findCategoryById(Long id) {
		EntityManager em = emf.createEntityManager();
		return (Category) em.createQuery("SELECT c FROM Category AS c WHERE c.id = :id").setParameter("id", id).getSingleResult();
	}

	@Override
	public Category updateCategory(Category category) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		try {
			transaction.begin();
			em.merge(category);
			em.flush();
			transaction.commit();
		}
		finally {
			if(transaction.isActive()) {
				transaction.rollback();
			}
			em.close();
		}
		return category;
	}

}
