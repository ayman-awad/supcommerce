package com.supinfo.supcommerce.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.supinfo.supcommerce.dao.jpa.JpaCategoryDao;
import com.supinfo.supcommerce.dao.jpa.JpaProductDao;

public class DaoFactory {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("sc_pu");
	
	public static ProductDao getProductDao() {
		return new JpaProductDao(emf);
	}
	
	public static CategoryDao getCategoryDao() {
		return new JpaCategoryDao(emf);
	}
	
}
