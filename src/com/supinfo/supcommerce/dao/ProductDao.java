package com.supinfo.supcommerce.dao;

import java.util.List;

import com.supinfo.supcommerce.entity.SupProduct;

public interface ProductDao {

	public List<SupProduct> getAllProducts();
	
	public SupProduct addProduct(SupProduct product);
	
	public void removeProduct(Long id);
	
	public void removeProduct(SupProduct product);
	
	public SupProduct findProductById(Long id);
	
}
