package com.supinfo.supcommerce.dao;

import java.util.List;

import com.supinfo.supcommerce.entity.Category;

public interface CategoryDao {

	public List<Category> getAllCategories();
	
	public Category addCategory(Category category);
	
	public void removeCategory(Long id);
	
	public void removeCategory(Category category);
	
	public Category findCategoryById(Long id);
	
	public Category updateCategory(Category category);
	
}
