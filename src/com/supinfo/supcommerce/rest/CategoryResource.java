package com.supinfo.supcommerce.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.supinfo.supcommerce.dao.DaoFactory;
import com.supinfo.supcommerce.entity.Category;

@Path("/categories")
public class CategoryResource {

	@GET
	public List<Category> getAllCategories() {
		return DaoFactory.getCategoryDao().getAllCategories();
	}
	
	@GET
	@Path("/{id}")
	public Category getCategory(@PathParam("id") Long id) {
		return DaoFactory.getCategoryDao().findCategoryById(id);
	}
	
	@POST
	public Category addCategory(Category category) {
		return DaoFactory.getCategoryDao().addCategory(category);
	}
	
	@PUT
	public Category updateCategory(Category category) {
		return DaoFactory.getCategoryDao().updateCategory(category);
	}
}
