package com.supinfo.supcommerce.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.supinfo.supcommerce.dao.DaoFactory;
import com.supinfo.supcommerce.entity.SupProduct;

@Path("/products")
public class ProductResource {

	// This should change to take advantage of JAX-B
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String getAllProductsInXml() {
		StringBuilder sb = new StringBuilder();
		sb.append("<products>");
		for (SupProduct product : DaoFactory.getProductDao().getAllProducts()) {
			sb.append(productToXml(product));
		}
		sb.append("</products>");
		return sb.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllProductsInJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"products\":[");
		for (SupProduct product : DaoFactory.getProductDao().getAllProducts()) {
			sb.append(productToJson(product)).append(",");
		}
		sb.replace(sb.lastIndexOf(","), sb.lastIndexOf(",")+1, "");
		sb.append("]}");
		return sb.toString();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public String getProductInXml(@PathParam("id") String id) {
		SupProduct product = getProduct(id);
		if(product != null) {
			return productToXml(product);
		}
		return "<error>No product found.</error>";
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getProductInJson(@PathParam("id") String id) {
		SupProduct product = getProduct(id);
		if(product != null) {
			return productToJson(product);
		}
		return "{\"error\":\"No product found.\"}";
	}
	
	@DELETE
	@Path("/{id}")
	public String removeProduct(@PathParam("id") String id) {
		try {
			long idLong = Long.parseLong(id);
			DaoFactory.getProductDao().removeProduct(idLong);
			return "Product removed.";
		}
		catch (NumberFormatException e) {
			return "Error: not a valid identifier.";
		}
	}
	
	private static String productToXml(SupProduct product) {
		return (new StringBuilder())
				.append("<product>")
					.append("<id>").append(product.getId()).append("</id>")
					.append("<name>").append(product.getName()).append("</name>")
					.append("<description>").append(product.getContent()).append("</description>")
					.append("<price>").append(product.getPrice()).append("</price>")
				.append("</product>")
				.toString();
	}
	
	private static String productToJson(SupProduct product) {
		return (new StringBuilder())
				.append("{")
					.append("\"id\":").append(product.getId()).append(",")
					.append("\"name\":\"").append(product.getName()).append("\",")
					.append("\"description\":\"").append(product.getContent()).append("\",")
					.append("\"price\":").append(product.getPrice())
				.append("}")
				.toString();
	}
	
	private SupProduct getProduct(String id) {
		try {
			long idLong = Long.parseLong(id);
			return DaoFactory.getProductDao().findProductById(idLong);
		}
		catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
}
